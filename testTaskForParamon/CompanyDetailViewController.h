//
//  CompanyDetailViewController.h
//  testTaskForParamon
//
//  Created by Владислав on 29.06.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *companyDetail;
@property (strong, nonatomic) NSString* companyId;
@property (strong, nonatomic) NSString* companyName;

@end
