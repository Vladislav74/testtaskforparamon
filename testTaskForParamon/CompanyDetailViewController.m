//
//  CompanyDetailViewController.m
//  testTaskForParamon
//
//  Created by Владислав on 29.06.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import "CompanyDetailViewController.h"
#import "DataObtainer.h"

@implementation CompanyDetailViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.companyDetail.text = @"";
    self.companyDetail.editable = NO;
    
    DataObtainer *dataObtainer = [[DataObtainer alloc] init];
    
    static NSString *urlBase = @"http://mobile.digital.paramon.ru:32080/test/?id=";
    NSString *const testUrl = [urlBase stringByAppendingString: self.companyId];
    
    //i not use json, because for some company(for example - Форсаж) server returns bad data, it for some test for candidats?
    [dataObtainer fetchDataFromUrl:testUrl Completetion:^(NSString *result, NSError *error) {
        if(error == nil) {
            
            NSString *detailData = [self convertRawCompanyDetailData:result];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.companyDetail.text = detailData;
            });

        } else {
            NSLog(@"error");
        }
    }];
}

-(NSString*)convertRawCompanyDetailData:(NSString*)rawString {
    NSRange searchResult = [rawString rangeOfString:@"description"];
    NSInteger lastIndex = [rawString length] - 3;
    NSInteger startIndexForCompanyDetailData = searchResult.location + 14;
    NSRange range = NSMakeRange(startIndexForCompanyDetailData, (lastIndex - startIndexForCompanyDetailData));
    NSString *companyDetailData = [rawString substringWithRange:range];
    return companyDetailData;
}

@end
