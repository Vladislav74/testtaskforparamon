//
//  DataObtainer.h
//  testTaskForParamon
//
//  Created by Владислав on 30.06.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataObtainer : NSObject

-(void)fetchJsonDataFromUrl:(NSString*)url Completetion:(void (^) (NSArray * result,NSError * error))completion;
-(void)fetchDataFromUrl:(NSString*)url Completetion:(void (^) (NSString * result,NSError * error))completion;

@end
