//
//  DataObtainer.m
//  testTaskForParamon
//
//  Created by Владислав on 30.06.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import "DataObtainer.h"

@implementation DataObtainer

-(void)fetchJsonDataFromUrl:(NSString*)url Completetion:(void (^) (NSArray * result,NSError * error))completion {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSURLSessionDataTask * dataTask = [
                                       [NSURLSession sharedSession]
                                       dataTaskWithRequest:request
                                       completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                           if(data == nil) {
                                               completion(nil,error);
                                               return;
                                           }
                                           NSError *myError;
                                           NSArray *array = [[NSArray alloc]initWithArray:[NSJSONSerialization                          JSONObjectWithData:data
                                                          options:kNilOptions error:&myError]];
                                           completion(array,myError);
                                       }];
    [dataTask resume];
}

-(void)fetchDataFromUrl:(NSString*)url Completetion:(void (^) (NSString * result,NSError * error))completion {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSURLSessionDataTask * dataTask = [
                                       [NSURLSession sharedSession]
                                       dataTaskWithRequest:request
                                       completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                           if(data == nil) {
                                               completion(nil,error);
                                               return;
                                           }
                                           NSError *myError;
                                           NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                           completion(dataString,myError);
                                       }];
    [dataTask resume];
}

@end
