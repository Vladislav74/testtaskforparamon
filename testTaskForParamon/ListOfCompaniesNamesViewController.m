//
//  ListOfCompaniesNamesViewController.m
//  testTaskForParamon
//
//  Created by Владислав on 29.06.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import "ListOfCompaniesNamesViewController.h"
#import "CompanyDetailViewController.h"
#import "DataObtainer.h"

@interface ListOfCompaniesNamesViewController()

@property (strong, nonatomic) NSArray* listOfCompanies;

@end

@implementation ListOfCompaniesNamesViewController

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    DataObtainer *dataObtainer = [[DataObtainer alloc] init];
    
    static NSString *testUrl = @"http://mobile.digital.paramon.ru:32080/test/";
    
    [dataObtainer fetchJsonDataFromUrl:testUrl Completetion:^(NSArray *result, NSError *error) {
        if(error == nil) {
            self.listOfCompanies = result;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
            
        } else {
            NSLog(@"error");
        }
    }];
}



#pragma mark - prepareForSegue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowCompanyDetails"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        CompanyDetailViewController *destViewController = segue.destinationViewController;
        NSDictionary *company = [self.listOfCompanies objectAtIndex:indexPath.row];
        NSString *companyName = [company objectForKey:@"name"];
        NSString *companyId = [company objectForKey:@"id"];
        destViewController.navigationItem.title = companyName;
        destViewController.companyId = companyId;
        destViewController.companyName = companyName;
    }
}

#pragma mark - table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.listOfCompanies count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cellIdentifier = @"CompanyNameCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if([self.listOfCompanies count] != 0) {
   
       NSDictionary *company = [self.listOfCompanies objectAtIndex:indexPath.row];
       NSString *companyName = [company objectForKey:@"name"];
    
       cell.textLabel.text = companyName;
    }
    return cell;
}

@end
